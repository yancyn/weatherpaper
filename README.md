# M5Paper Weather Station
This M5Paper weather station is refered modified from https://github.com/tuenhidiy/m5paper-weatherstation.

![Explain Display](https://user-images.githubusercontent.com/34854662/108235713-aef15d80-7178-11eb-85f6-2d1df97b6e43.png)

## Getting Started
1. Use VS Code as your IDE.
2. Install platformio extension.
3. Build > Upload.
4. Done.

## How to use
Change your Wi-Fi parameters in `src/WiFiInfo.h`

Weather, temmperature, humidity, visibility, pressure, wind ... icons are converted to "C" codes and located at:

- `scr/WeatherIcons.c`
- `scr/THPIcons.c`
- `scr/WindIcons.c`

## Buttons
- BtnL: Shutdown
- BtnR: Refresh e-paper
- BtnP: Time Synchronization with a NTP server

## References
1. Instructables: https://www.instructables.com/M5Paper-Weather-Station/
2. YouTube: https://www.youtube.com/watch?v=Mbq6BIsMcAs